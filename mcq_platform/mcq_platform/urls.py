from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'mcq_platform.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^mcq/', include('mcq.urls',namespace ="mcq")),
    url(r'^admin/', include(admin.site.urls)),
]
