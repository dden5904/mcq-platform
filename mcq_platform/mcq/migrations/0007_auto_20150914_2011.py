# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mcq', '0006_choiceasststats'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignedasst',
            name='from_user',
            field=models.ForeignKey(related_name='from_user', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='assignedasst',
            name='user',
            field=models.ForeignKey(related_name='to_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
