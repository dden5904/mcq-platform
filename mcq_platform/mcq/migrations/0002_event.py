# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mcq', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('event', models.CharField(max_length=b'50')),
                ('when', models.DateTimeField(verbose_name=b'Date happened')),
                ('from_user', models.ForeignKey(related_name='relationships', to=settings.AUTH_USER_MODEL)),
                ('to_user', models.ForeignKey(related_name='related_to', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
