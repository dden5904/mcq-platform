# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mcq', '0007_auto_20150914_2011'),
    ]

    operations = [
        migrations.CreateModel(
            name='AllocationRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('when', models.DateTimeField(verbose_name=b'Date allocated')),
                ('asst', models.ForeignKey(to='mcq.Assessment')),
                ('staff', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
