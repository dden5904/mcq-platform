# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Assessment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('assessment_name', models.CharField(default=0, max_length=100)),
                ('assessment_type', models.IntegerField(default=0, choices=[(0, b'practical exercise'), (1, b'formal exam')])),
                ('assessment_description', models.CharField(default=b'undefined', max_length=200)),
                ('duration', models.IntegerField(default=0)),
                ('mcq_num', models.IntegerField(default=0)),
                ('points', models.IntegerField(default=0)),
                ('pub_date', models.DateTimeField(default=None, verbose_name=b'date published')),
                ('deadline', models.DateTimeField(verbose_name=b'deadline')),
                ('status', models.IntegerField(default=0, choices=[(0, b'OPEN'), (1, b'CLOSED')])),
            ],
        ),
        migrations.CreateModel(
            name='AssignedAsst',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_done', models.BooleanField(default=False)),
                ('asst', models.ForeignKey(to='mcq.Assessment')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='AssignedGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('asst', models.ForeignKey(to='mcq.Assessment')),
            ],
        ),
        migrations.CreateModel(
            name='AsstResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('score', models.IntegerField(default=0)),
                ('grade', models.CharField(default=b'Undefined', max_length=50)),
                ('assessment', models.ForeignKey(to='mcq.Assessment')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Bundle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bundle_name', models.CharField(max_length=100)),
                ('bundle_desc', models.CharField(default=b'undefined', max_length=200)),
                ('pub_date', models.DateTimeField(verbose_name=b'date published')),
                ('total_question_num', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_code', models.CharField(default=b'11', max_length=8)),
                ('category_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer_code', models.CharField(max_length=10)),
                ('is_correct', models.BooleanField(default=False)),
                ('choice_text', models.CharField(max_length=200)),
                ('votes', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('group_name', models.CharField(max_length=20)),
                ('creation_date', models.DateTimeField(verbose_name=b'date published')),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_text', models.CharField(max_length=200)),
                ('pub_date', models.DateTimeField(verbose_name=b'date published')),
                ('question_type', models.IntegerField(default=0, choices=[(0, b'Single Answers'), (1, b'Multiple Answers'), (2, b'True/False'), (3, b'Input')])),
                ('correct_answer', models.CharField(default=0, max_length=100)),
                ('explanation', models.CharField(default=b'undefined', max_length=100)),
                ('input_answer', models.CharField(default=0, max_length=100)),
                ('hints', models.CharField(default=b'No hints.', max_length=100)),
                ('category', models.ManyToManyField(to='mcq.Category')),
            ],
        ),
        migrations.CreateModel(
            name='QuestionAttempt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_answer', models.CharField(max_length=10)),
                ('is_correct', models.NullBooleanField()),
                ('assessment', models.ForeignKey(to='mcq.Assessment')),
                ('question', models.ForeignKey(to='mcq.Question')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num_completed_asst', models.IntegerField(default=0)),
                ('num_incompelete_asst', models.IntegerField(default=0)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='choice',
            name='question',
            field=models.ForeignKey(to='mcq.Question'),
        ),
        migrations.AddField(
            model_name='bundle',
            name='question',
            field=models.ManyToManyField(to='mcq.Question'),
        ),
        migrations.AddField(
            model_name='assignedgroup',
            name='group',
            field=models.ForeignKey(to='mcq.group'),
        ),
        migrations.AddField(
            model_name='assessment',
            name='bundle',
            field=models.ManyToManyField(to='mcq.Bundle'),
        ),
        migrations.AddField(
            model_name='assessment',
            name='category',
            field=models.ForeignKey(to='mcq.Category', null=True),
        ),
        migrations.AddField(
            model_name='assessment',
            name='publisher',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
