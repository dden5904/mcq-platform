# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mcq', '0003_auto_20150827_0057'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssessmentQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_type', models.IntegerField(default=4)),
            ],
        ),
        migrations.RemoveField(
            model_name='assessment',
            name='bundle',
        ),
        migrations.AddField(
            model_name='question',
            name='is_replica',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_type',
            field=models.IntegerField(default=0, choices=[(0, b'Single Answers'), (1, b'Multiple Answers'), (2, b'True/False'), (3, b'Input'), (4, b'undefined')]),
        ),
        migrations.AddField(
            model_name='assessmentquestion',
            name='asst_question',
            field=models.ForeignKey(to='mcq.Assessment'),
        ),
        migrations.AddField(
            model_name='assessmentquestion',
            name='question',
            field=models.ForeignKey(to='mcq.Question'),
        ),
    ]
