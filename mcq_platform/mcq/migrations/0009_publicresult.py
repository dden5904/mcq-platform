# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mcq', '0008_allocationrecord'),
    ]

    operations = [
        migrations.CreateModel(
            name='PublicResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num_of_correct_attempts', models.IntegerField(default=0)),
                ('num_of_incorrect_attempts', models.IntegerField(default=0)),
                ('question', models.ForeignKey(to='mcq.Question')),
            ],
        ),
    ]
