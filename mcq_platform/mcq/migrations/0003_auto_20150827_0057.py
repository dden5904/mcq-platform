# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mcq', '0002_event'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='num_incompelete_asst',
            new_name='num_incomplete_asst',
        ),
    ]
