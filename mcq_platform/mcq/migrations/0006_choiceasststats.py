# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mcq', '0005_assessment_question'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChoiceAsstStats',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('votes', models.IntegerField(default=0)),
                ('asst', models.ForeignKey(to='mcq.Assessment')),
                ('choice', models.ForeignKey(to='mcq.Choice')),
                ('question', models.ForeignKey(to='mcq.Question')),
            ],
        ),
    ]
