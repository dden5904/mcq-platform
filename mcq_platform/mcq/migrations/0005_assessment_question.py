# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mcq', '0004_auto_20150901_0102'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessment',
            name='question',
            field=models.ManyToManyField(related_name='asstquestion', to='mcq.Question'),
        ),
    ]
