from django.contrib import admin

# Register your models here.
from .models import Choice, Question, Category, Bundle,QuestionAttempt,AsstResult,Assessment,AssignedAsst,Event,ChoiceAsstStats, AllocationRecord,PublicResult

from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from mcq.models import UserProfile

# Define an inline admin descriptor for UserProfile model
# which acts a bit like a singleton
class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'profile'

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)




class AssignedasstInline(admin.TabularInline):
    model = AssignedAsst
    can_delete = True
    verbose_name_plural = 'assigned'


class AssessmentAdmin(admin.ModelAdmin):
    inlines = (AssignedasstInline,)

admin.site.register(Assessment,AssessmentAdmin)

admin.site.register(AssignedAsst)



class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class AttemptInline(admin.TabularInline):
    model = QuestionAttempt
    extra = 3

class QuestionAdmin(admin.ModelAdmin):

    fieldsets = [
        (None,               {'fields': ['question_text']}),
        (None,               {'fields': ['question_type']}),
        (None,               {'fields': ['category']}),
        (None,               {'fields': ['correct_answer']}),
        (None,               {'fields': ['input_answer']}),
         (None,               {'fields': ['is_replica']}),
        
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    #inlines = [AttemptInline]

    list_display = ('question_text', 'pub_date','was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']





admin.site.register(PublicResult)

admin.site.register(AllocationRecord)

admin.site.register(Question, QuestionAdmin)

admin.site.register(Category)

admin.site.register(Bundle)

admin.site.register(QuestionAttempt)

admin.site.register(AsstResult)

admin.site.register(Choice)

admin.site.register(Event)

admin.site.register(ChoiceAsstStats)
