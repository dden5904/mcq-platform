from django.shortcuts import get_object_or_404, render

# Create your views here.
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext, loader
from django.http import Http404
from .models import Question, Choice, Category, Bundle, Assessment,QuestionAttempt,AssignedAsst,AsstResult,Event,UserProfile,AllocationRecord,PublicResult
from django.db import models
from django.db.models import Count
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.views import generic


from django.utils import timezone

from django_ajax.decorators import ajax

from django.shortcuts import render, render_to_response


from django.db import transaction
from django.db import IntegrityError






from django.views.decorators.csrf import csrf_exempt

import json


from itertools import chain

from django.db.models import Count, Avg

@transaction.atomic
def EmbedMcq(request,question_id):
    question = get_object_or_404(Question, pk=question_id)
    errors = []
    if request.method == 'POST':
        try:
            with transaction.atomic(): 
                choiceSet =  Choice.objects.all().filter(question=question).order_by('answer_code')
                if question.question_type == 1:
                    user_answer_list = request.POST.getlist(question_id)
                    print(user_answer_list)

                    for answer in user_answer_list:
                        for choice in choiceSet:
                            if answer == choice.answer_code:
                                choice.votes += 1
                                choice.save()

                    user_answer=""
                    for code in user_answer_list:
                        user_answer = user_answer+code

                    instance = PublicResult.objects.get(question=question)
                    if question.correct_answer == user_answer:
                        instance.num_of_correct_attempts += 1
                        instance.save()
                    else:
                        instance.num_of_incorrect_attempts += 1
                        instance.save()

                return render_to_response('embed/embed_success.html', locals(),context_instance=RequestContext(request))

    
        except IntegrityError as e:
            errors.append(e.message)
            return render_to_response('embed/embed_mcq.html', locals(),context_instance=RequestContext(request))


    return render_to_response('embed/embed_mcq.html', locals(),context_instance=RequestContext(request))




def tt(request):
 return render_to_response('mcq/ajax.html')


names=list();
names.append("zhangsa")
names.append("aa")
names.append("b")
names.append("c")


@csrf_exempt
def ccc(request):
    print("sss")
    name=request.POST.get("name",None)

    rtxt="";
    print(name)
    if name is not None:
        b = name in names
        if b:
            rtxt="name does exist"
        else:
            print("name does not exist")
            rtxt="name does not exist"
    print(rtxt)
    return HttpResponse(json.dumps({"msg":rtxt}))

@csrf_exempt
def delete(request):    
    print(request.POST.get("id"))
    question = Question.objects.get(pk=request.POST.get("id"))
    bundle = get_object_or_404(Bundle, pk=request.POST.get("bundle"))
    bundle.question.remove(question)
    payload = {'success': True}
    return HttpResponse(json.dumps(payload), content_type='application/json')





def index3(request):
    return render_to_response('mcq/ex.html', {'message': 'Hello World!'})

def hello(request):
    return HttpResponse('Hello Client!')

def check(request,loginame):
    return HttpResponse('Hello Client!')

def index2(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'mcq/index.html', context)

def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'mcq/detail.html', {'question': question})

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'mcq/results.html', {'question': question})




def profile(request,para):

    request_user = User.objects.get(id=para)



    return render(request, 'mcq/user_profile.html',{'user':request_user})




def  AdminIndex(request):

    asst_list =[]

    num_register_days = 0
    event_list = []


    p = AssignedAsst.objects.select_related('asst').select_related('from_user').filter(from_user=request.user).count()


    #incomplete= AssignedAsst.objects.filter(from_user=request.user,is_done=False).values('asst').annotate(incomplete_num=Count('asst'))
    #complete = AssignedAsst.objects.filter(from_user=request.user,is_done=True).values('asst').annotate(complete_num=Count('asst'))
    

    if request.user.is_authenticated():
        allocated_list = AllocationRecord.objects.filter(staff=request.user).order_by("when")

        asst_list = AssignedAsst.objects.filter(user = request.user,asst__deadline__gte =timezone.now(),is_done=False).order_by('asst__deadline')
        num_register_days = (request.user.last_login- request.user.date_joined).days
        num_question_created = Question.objects.all().count()
        num_asst_created = Assessment.objects.filter(publisher=request.user).count()

    return render_to_response('mcq/index.html', locals(),context_instance=RequestContext(request))



class AdminView(generic.ListView):
    template_name = 'mcq/index.html'
    context_object_name = 'subject_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return Category.objects.order_by("category_code")

def InstallInitalData():

    User.objects.create_superuser(username='dengda', email="example@example.com",password='123123')



def  index(request):

    asst_list =[]

    num_register_days = 0
    event_list = []



    if request.user.is_authenticated():
        asst_list = AssignedAsst.objects.filter(user = request.user,asst__deadline__gte =timezone.now(),is_done=False).order_by('asst__deadline')
        num_register_days = (request.user.last_login- request.user.date_joined).days
        event_list = Event.objects.filter(to_user = request.user).order_by('-when')[:5]

 

    return render_to_response('mcq/user_index.html', {'asst_list': asst_list,'num_register_days':num_register_days,'event_list':event_list},context_instance=RequestContext(request))




class IndexView2(generic.ListView):
    template_name = 'mcq/user_index.html'
    context_object_name = 'subject_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return Category.objects.order_by("category_code")



def  SubjectCreation(request):

    errors= []
    subjectName=None
    subjectCode=None


    if request.method == 'POST':


        if not request.POST.get('subjectName'):
            errors.append('Please fill out subject name.')
        else:
            subjectName = request.POST.get('subjectName')

        if not request.POST.get('subjectCode'):
            errors.append('Please fill out subject code.')
        else:
            subjectCode= request.POST.get('subjectCode')
       


        if subjectCode is not None and subjectName is not None :


            new_subject = Category()
            new_subject.category_code = subjectCode
            new_subject.category_name = subjectName
            new_subject.save()
      
            return HttpResponseRedirect('/mcq/admin/labellist')
    

    return render_to_response('mcq/label_creation.html', {'errors': errors},context_instance=RequestContext(request))


def  LabelEdit(request,label_id):

    label = get_object_or_404(Category, pk=label_id)
    errors= []
    subjectName=None
    subjectCode=None

    print(label)
    if request.method == 'POST':


        if not request.POST.get('subjectName'):
            errors.append('Please fill out subject name.')
        else:
            subjectName = request.POST.get('subjectName')

        if not request.POST.get('subjectCode'):
            errors.append('Please fill out subject code.')
        else:
            subjectCode= request.POST.get('subjectCode')
       


        if subjectCode is not None and subjectName is not None :


            label.category_code = subjectCode
            label.category_name = subjectName
            label.save()
      
            return HttpResponseRedirect('/mcq/admin/labellist')
    

    return render_to_response('mcq/label_edit.html', {'errors': errors,'label':label},context_instance=RequestContext(request))





def  McqOption(request):


    errors= []
    optionsRadios = None

    if request.method == 'POST':
        if not request.POST.get('optionsRadios'):
            errors.append('Please choose one MCQ type.')
        else:
            optionsRadios = request.POST.get('optionsRadios')
            print('optionsRadios',optionsRadios)
       


        if optionsRadios is not None:

            if optionsRadios == 'option0':
                return HttpResponseRedirect('/mcq/admin/mcqcollection/create_mcq/0' )
            if optionsRadios == 'option1':
                return HttpResponseRedirect('/mcq/admin/mcqcollection/create_mcq/1')
            if optionsRadios == 'option2':
                return HttpResponseRedirect('/mcq/admin/mcqcollection/create_mcq/2')
            if optionsRadios == 'option3':
                return HttpResponseRedirect('/mcq/admin/mcqcollection/create_mcq/3')


    return render_to_response('mcq/create_mcq.html', {'errors': errors},context_instance=RequestContext(request))


@transaction.atomic
def AsstEdit(request,asst_id):

    asst = get_object_or_404(Assessment, pk=asst_id)

    errors = []
    subject_list = Category.objects.order_by("category_code")
 
    question_list = Question.objects.order_by("category").all()

    questionSet =  asst.question.all()
    asstTitle = None
    asstType = None
    asstDesc = None
    asstDuration = None
    asstSubject = None
    asstScore = 0
    asstMcqnum = 0

    questionList = []
    numBundle = 0
    deadline = None
    questions = []

    if request.method == 'POST':
        if not request.POST.get('asstTitle'):
            errors.append('Please fill out assessment title.')
        else:
            asstTitle = request.POST.get('asstTitle')

        if not request.POST.get('asstType'):
            errors.append('Please fill out assessment type.')
        else:
            asstType = request.POST.get('asstType')

        if not request.POST.get('asstDesc'):
            errors.append('Please select at least two MCQ.') 
        else:
            asstDesc = request.POST.get('asstDesc')     

        if not request.POST.getlist('asstQuestion'):
            errors.append('Please select bundle.') 
        else:
            questionList = request.POST.getlist('asstQuestion')   
      
        if not request.POST.get('asstDuration'):
            errors.append('Please select duration.') 
        else:
            asstDuration = request.POST.get('asstDuration')     

        if not request.POST.get('asstSubject'):
            errors.append('Please select duration.') 
        else:
            asstSubject = request.POST.get('asstSubject')   

        if not request.POST.get('deadline'):
            errors.append('Please select deadline.') 
        else:
            deadline = request.POST.get('deadline') 

    
        try:
            with transaction.atomic():

                asst.assessment_name = asstTitle
                asst.assessment_type = asstType
                asst.assessment_description = asstDesc
                asst.duration = asstDuration
                asst.pub_date = timezone.now()
                asst.deadline = deadline
                asst.publisher = request.user
                asst.category = Category.objects.get(pk=asstSubject)
                asst.save()
                print("dd")
                print(questionList)
                for question in questionSet:
                   asst.question.remove(question)   

                for question in questionList:
                    asst.question.add(question)
      

                counter = asst.question.all().count()
                asst.mcq_num = counter
                asst.points  =  counter
                asst.save()

      
                return HttpResponseRedirect('/mcq/admin/statistics/asst/%s/' %asst.id)


        except IntegrityError as e:
            errors.append(e.message)
            return render_to_response('mcq/asst_edit.html', locals(),context_instance=RequestContext(request))

    return render_to_response('mcq/asst_edit.html', locals(),context_instance=RequestContext(request))




@transaction.atomic
def AsstCreation(request):

    errors = []
    subject_list = Category.objects.order_by("category_code")
    bundle_list = Bundle.objects.order_by("pub_date").all()
    question_list = Question.objects.order_by("category").all()
    mcqList = request.POST.getlist('mcqselection')

    asstTitle = None
    asstType = None
    asstDesc = None
    asstDuration = None
    asstSubject = None
    asstScore = 0
    asstMcqnum = 0
    bundleList = []
    questionList = []
    numBundle = 0
    deadline = None
    selectquestionoption = None
    bundle = None
    questions = []

    if request.method == 'POST':
        if not request.POST.get('asstTitle'):
            errors.append('Please fill out assessment title.')
        else:
            asstTitle = request.POST.get('asstTitle')

        if not request.POST.get('asstType'):
            errors.append('Please fill out assessment type.')
        else:
            asstType = request.POST.get('asstType')

        if not request.POST.get('asstDesc'):
            errors.append('Please select at least two MCQ.') 
        else:
            asstDesc = request.POST.get('asstDesc')          
       

        if not request.POST.get('asstDuration'):
            errors.append('Please select duration.') 
        else:
            asstDuration = request.POST.get('asstDuration')     

        if not request.POST.get('asstSubject'):
            errors.append('Please select duration.') 
        else:
            asstSubject = request.POST.get('asstSubject')   

        if not request.POST.get('deadline'):
            errors.append('Please select deadline.') 
        else:
            deadline = request.POST.get('deadline') 

        if not request.POST.get('selectquestionoption'):
            errors.append('Please select question options.') 
        else:
            selectquestionoption  = request.POST.get('selectquestionoption')

        if selectquestionoption == "1":
            if not request.POST.getlist('asstBundle'):
                errors.append('Please select bundle.') 
            else:
                bundleList = request.POST.getlist('asstBundle')    

        if selectquestionoption == "2":
            if not request.POST.getlist('asstQuestion'):
                errors.append('Please select bundle.') 
            else:
                questionList = request.POST.getlist('asstQuestion')   

        if selectquestionoption == "3":
            if not request.POST.getlist('asstBundle'):
                errors.append('Please select bundle.') 
            else:
                bundleList = request.POST.getlist('asstBundle')    

            if not request.POST.getlist('asstQuestion'):
                errors.append('Please select bundle.') 
            else:
                questionList = request.POST.getlist('asstQuestion')             
        try:
            with transaction.atomic():
                print("yes")
                new_asst = Assessment()
                new_asst.assessment_name = asstTitle
                new_asst.assessment_type = asstType
                new_asst.assessment_description = asstDesc
                new_asst.duration = asstDuration
                new_asst.pub_date = timezone.now()
                new_asst.deadline = deadline
                new_asst.publisher = request.user
                new_asst.category = Category.objects.get(pk=asstSubject)
                new_asst.save()
                if selectquestionoption =="1" : #Select questions from bundle
                    print("bundle")

                    for bundle in bundleList:
                        bundle = Bundle.objects.get(pk=bundle)
                        questions = bundle.question.all()
                        new_asst.question = questions

                    counter = new_asst.question.all().count()
                    print(counter)
                    new_asst.mcq_num = counter
                    new_asst.points  =  counter
                    new_asst.save()
                    return HttpResponseRedirect('/mcq/admin/statistics/asst/%s/' %new_asst.id)


                if selectquestionoption == "2" : #Select questions from MCQ collection

                    for question in questionList:
                        new_asst.question.add(question)
      

                    counter = new_asst.question.all().count()
                    new_asst.mcq_num = counter
                    new_asst.points  =  counter
                    new_asst.save()

      
                    return HttpResponseRedirect('/mcq/admin/statistics/asst/%s/' %new_asst.id)

                if selectquestionoption == "3" : #Select questions from both
                    questions = None
                    for bundle in bundleList:
                        bundle = Bundle.objects.get(pk=bundle)
                        questions = bundle.question.all()
                        new_asst.question = questions

                    for question in questionList:
                        if not new_asst.question.filter(id=question).exists():
                            new_asst.question.add(question)
      

                    counter = new_asst.question.count()
                    new_asst.mcq_num = counter
                    new_asst.points  =  counter
                    new_asst.save()

      
                    return HttpResponseRedirect('/mcq/admin/statistics/asst/%s/' %new_asst.id)
        except IntegrityError as e:
            errors.append(e.message)
            return render_to_response('mcq/asst_creation.html', {'errors': errors,'subject_list':subject_list,'bundle_list':bundle_list,'question_list':question_list},context_instance=RequestContext(request))

    return render_to_response('mcq/asst_creation.html', {'errors': errors,'subject_list':subject_list,'bundle_list':bundle_list,'question_list':question_list},context_instance=RequestContext(request))



class AsstCollectionView(generic.ListView):
    template_name = 'mcq/asst_list.html'
    context_object_name = 'asst_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return Assessment.objects.all()


@transaction.atomic
def AsstAllocation(request):

    errors = []
    asst_list = Assessment.objects.all().order_by("id")
    user_list = User.objects.all().order_by("username")

    selected_asst = None
    userselection = None

    if request.method == 'POST':
        if not request.POST.get('asstselection'):
            errors.append('Please fill out assessment selection.')
        else:
            selected_asst = request.POST.get('asstselection')

        if not request.POST.getlist('userselection'):
            errors.append('Please fill out user selection.')
        else:
            userselection = request.POST.getlist('userselection')

        if selected_asst is not None and userselection is not None:
            asst = get_object_or_404(Assessment, pk=selected_asst)

            for user in userselection:
                print(AssignedAsst.objects.filter(asst=asst,user=user))

                if not AssignedAsst.objects.filter(asst=asst,user=user):

                    user_instance = User.objects.get(pk=user)
                    new_assigned_asst = AssignedAsst()
                    new_assigned_asst.asst = asst
                    new_assigned_asst.user = user_instance
                    new_assigned_asst.from_user = request.user
                    new_assigned_asst.save()
                    user_instance.userprofile.num_incomplete_asst += 1
                    user_instance.userprofile.save()



                    new_event = Event()
                    new_event.from_user = request.user
                    new_event.to_user = user_instance
                    new_event.event = "allocated a new assessment to you."
                    new_event.when = timezone.now()
                    new_event.save()

                    new_record = AllocationRecord()
                    new_record.asst = asst
                    new_record.staff = request.user
                    new_record.when = timezone.now()
                    new_record.save()

                       

      
            return HttpResponseRedirect('/mcq/admin/asstcollection')

    return render_to_response('mcq/asst_allocation.html', {'errors': errors,'asst_list':asst_list,'user_list':user_list},context_instance=RequestContext(request))




def BundleEdit(request,bundle_id):

    errors =[]
    bundle = get_object_or_404(Bundle, pk=bundle_id)
    questionSet =  bundle.question.all()
    subject_list = Category.objects.order_by("category_code")
    mcq_list = Question.objects.order_by("category").all()

    bundleName = None
    description = None
    mcqList = request.POST.getlist('mcqselection')
    print(questionSet)


    if request.method == 'POST':
        if not request.POST.get('bundlename'):
            errors.append('Please fill out bundle name.')
        else:
            bundleName = request.POST.get('bundlename')

        if not request.POST.get('description'):
            errors.append('Please fill out description.')
        else:
            description = request.POST.get('description')

        if not request.POST.getlist('mcqselection'):
            errors.append('Please select at least two MCQ.') 
        else:
            mcqList = request.POST.getlist('mcqselection')          
       


        if bundleName is not None and description is not None and mcqList is not None:

            bundle.bundle_name = bundleName
            bundle.bundle_desc = description
            bundle.pub_date = timezone.now()
            bundle.total_question_num = len(mcqList)
            bundle.save()
            bundle.question = mcqList
            bundle.save()
      
            return HttpResponseRedirect('/mcq/admin/bundlecollection/%s/' %bundle.id)

    return render_to_response('mcq/bundle_edit.html', {'errors': errors,'subject_list':subject_list,'mcq_list':mcq_list,'bundle':bundle,'questionSet':questionSet},context_instance=RequestContext(request))





def BundleCreation(request):

    errors =[]
    subject_list = Category.objects.order_by("category_code")
    mcq_list = Question.objects.order_by("category").all()

    bundleName = None
    description = None
    mcqList = request.POST.getlist('mcqselection')
    print(mcqList)


    if request.method == 'POST':
        if not request.POST.get('bundlename'):
            errors.append('Please fill out bundle name.')
        else:
            bundleName = request.POST.get('bundlename')

        if not request.POST.get('description'):
            errors.append('Please fill out description.')
        else:
            description = request.POST.get('description')

        if not request.POST.getlist('mcqselection'):
            errors.append('Please select at least two MCQ.') 
        else:
            mcqList = request.POST.getlist('mcqselection')          
       


        if bundleName is not None and description is not None and mcqList is not None:


            new_bundle = Bundle()
            new_bundle.bundle_name = bundleName
            new_bundle.bundle_desc = description
            new_bundle.pub_date = timezone.now()
            new_bundle.total_question_num = len(mcqList)
            new_bundle.save()
            new_bundle.question = mcqList
            new_bundle.save()
      
            return HttpResponseRedirect('/mcq/admin/bundlecollection')

    return render_to_response('mcq/bundle_creation.html', {'errors': errors,'subject_list':subject_list,'mcq_list':mcq_list},context_instance=RequestContext(request))





def DistinctFilter(initial):
    questionSet = None

    bundleList = asst.bundle.all()
    q=[]    
    for bundle in bundleList:
        q.append(bundle.question.all())
        

    for i in range(0,len(q)):
        if(len(q) == 1):
            questionSet = q[i]

        if(i+1 <len(q)):
            questionSet = q[i]|q[i+1]

    questionSet = questionSet.distinct()
    return questionSet


def DistinctQuestion(asst):
    questionSet = None

    bundleList = asst.bundle.all()
    q=[]    
    for bundle in bundleList:
        q.append(bundle.question.all())
        

    for i in range(0,len(q)):
        if(len(q) == 1):
            questionSet = q[i]

        if(i+1 <len(q)):
            questionSet = q[i]|q[i+1]

    questionSet = questionSet.distinct()
    return questionSet



def Transfer(para):
      alphaList = ['','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
      return alphaList[para]

@transaction.atomic
def  McqCreation(request, option):


    subject_list = Category.objects.order_by("category_code")

    errors= []

    question_type = option
    category = None
    question_text = None
    explanation = None

    hint = None


    choice_counter = None
    choice_text = None
    correct_answer = None
    answer_code = None
    explanation = None
    answer_list = ""



    if request.method == 'POST':

        if not request.POST.get('subject'):
            errors.append('Please fill out subject name.')
        else:
            category = request.POST.get('subject')

        if not request.POST.get('questiontext'):
            errors.append('Please fill out question text.')
        else:
            question_text = request.POST.get('questiontext')

        if not request.POST.get('explanation'):
            errors.append('Please fill out explanation.')
        else:
            explanation = request.POST.get('explanation')


        if question_type is not None:
            
            try:
                with transaction.atomic():
                    new_mcq = Question()
                    new_mcq.question_text = question_text
                    new_mcq.pub_date = timezone.now()
                    new_mcq.question_type = question_type
                    new_mcq.explanation = explanation
                    new_mcq.save()
                    category = Category.objects.filter(category_code=category)
                    new_mcq.category = category
                    new_mcq.save()

                    if question_type == '0' or question_type == '2':
                        if request.POST.get('counter'):
                           
                            choice_counter = request.POST.get('counter')
                            b = int(choice_counter)
                            print(b)
                            for i in range(1,int(b)+1):
                   
                                new_choice = Choice()
                                new_choice.question = new_mcq
                               
                                correct_answer = request.POST.get('optionsRadios')
                                code = Transfer(i)
                                new_choice.answer_code = code

                                choice_str = "choicetext"+str(i)
                                choice_text = request.POST.get(choice_str) 
                                new_choice.choice_text = choice_text
                                print(choice_text)
                                print(i)
                                if "option"+str(i) == correct_answer:
                                    new_choice.is_correct = True
                                    answer_list = answer_list+code
                                else:
                                    new_choice.is_correct = False
                                new_choice.save()
                            new_mcq.correct_answer = answer_list
                            new_mcq.save()
                            print(answer_list)

                        return HttpResponseRedirect('/mcq/admin/statistics/mcq/%s/' %new_mcq.id)
                    if question_type == '1':
                        if request.POST.get('counter'):
                            choice_counter = request.POST.get('counter')
                            b = int(choice_counter)
                            print(b)
                            for i in range(1,int(b)+1):
                                print(i)
                                new_choice = Choice()
                                new_choice.question = new_mcq

                                code = Transfer(i)
                                print(code)
                                new_choice.answer_code = code

                                choice_str = "choicetext"+str(i)
                                choice_text = request.POST.get(choice_str) 
                                new_choice.choice_text = choice_text

                                answer_str = "optionsCheckbox"+str(i)

                                if not request.POST.get(answer_str):
                                    new_choice.is_correct = False
                                else:
                                    new_choice.is_correct = True
                                    answer_list = answer_list+code

                                new_choice.save()
                            new_mcq.correct_answer = answer_list
                            print(answer_list)
                            new_mcq.save()

                        return HttpResponseRedirect('/mcq/admin/statistics/mcq/%s/' %new_mcq.id)

                    if question_type == '3':

                        new_mcq.input_answer = request.POST.get('inputanswer')
                        new_mcq.save()

                        return HttpResponseRedirect('/mcq/admin/statistics/mcq/%s/' %new_mcq.id)


            except IntegrityError as e:
                errors.append(e.message)
                return render_to_response('mcq/mcq_creation.html',{'errors': errors,'optionNum':option,'subject_list':subject_list},context_instance=RequestContext(request))



    return render_to_response('mcq/mcq_creation.html', {'errors': errors,'optionNum':question_type,'subject_list':subject_list},context_instance=RequestContext(request))



class AsstListView(generic.ListView):
    template_name = 'mcq/user_asst_list.html'
    context_object_name = 'asst_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return AssignedAsst.objects.filter(user=self.request.user.pk,is_done=False)


class AsstResultView(generic.ListView):
    template_name = 'mcq/user_asst_result_list.html'
    context_object_name = 'asst_result'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return AssignedAsst.objects.filter(user=self.request.user.pk,is_done=True)


def SubjectListView(request):
    Category.objects.order_by("category_code")


def SubjectListView(request):
    subject_list = Category.objects.order_by("category_code")

    return render(request, 'mcq/label_list.html', locals())


class SubjectListView1(generic.ListView):
    template_name = 'mcq/label_list.html'
    context_object_name = 'subject_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return Category.objects.order_by("category_code")



class IndexView2(generic.ListView):
    template_name = 'mcq/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return Question.objects.filter(
            pub_date__lte=timezone.now()
            ).order_by('-pub_date')[:5]


class McqCollectionView(generic.ListView):
    template_name = 'mcq/mcq_list.html'
    context_object_name = 'mcq_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return Question.objects.all().order_by('pub_date')


class BundleCollectionView(generic.ListView):
    template_name = 'mcq/bundle_list.html'
    context_object_name = 'bundle_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        lte:less than or equal to
        """
        return Bundle.objects.all()

class BundleDetailView(generic.DetailView):
    model = Bundle
    template_name = 'mcq/bundle_detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Bundle.objects.filter(pk=model.pk)

class DetailView(generic.DetailView):
    model = Question
    template_name = 'mcq/detail.html'
    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pk=Model.pk)

def DifficultyLevel(para):
    if para < 5:
        return "Extremely difficult"
    if para >= 6 and para <= 10:
        return "Very difficult"
    if para >= 11 and para <= 20:
        return "Difficult"
    if para >= 20 and para <= 34:
        return "Moderately difficult"
    if para >= 35 and para <= 64:
        return "About right for the average student"
    if para >= 65 and para <= 80:
        return "Fairly easy"
    if para >= 81 and para <= 89:
        return "Easy"
    if para >= 90 and para <= 94:
        return "Very easy"
    if para >= 95 and para <= 100:
        return "Extremely easy"
    return "N/E"


@csrf_exempt
#results["choice",attempts,question_pk,percentage,is_Correct,correct_percentage]
def AsstStatsView(request, assessment_id):
    asst = get_object_or_404(Assessment,pk = assessment_id)


    if request.POST.has_key('delete'):
        question = get_object_or_404(Question, pk=request.POST.get("questionId"))
        asst.question.remove(question)

        return HttpResponseRedirect('/mcq/admin/statistics/asst/%s/' %asst.id)

    attempSet = None
    num_respondents = 0
    stats = []
    index_stats = []

    num_completed = 0
    num_incompleted = 0
    complete_ratio = 0
    incomplet_ratio = 100 

    rankingList = AsstResult.objects.filter(assessment=asst).order_by("score")


    questionSet = []


    questionSet = asst.question.all()


    assigned = AssignedAsst.objects.filter(asst= asst).order_by("-is_done")
    assignedList = assigned.values_list('user_id', flat=True)
    print(assignedList)
    num_respondents = assigned.count()
    num_completed = assigned.filter(is_done=True).count()
    num_incompleted = num_respondents - num_completed 

    if num_respondents > 0:
        complete_ratio = int((num_completed/float(num_respondents)) * 100)
        incomplete_ratio = 100 -complete_ratio


    correct_responses = 0
    index = []

    num_responses = 0
    correct_ratio = 0

    for question in questionSet:
        
        attemptSet = QuestionAttempt.objects.all().filter(question = question,assessment=asst,user_id__in=assignedList)

        choiceSet =  Choice.objects.all().filter(question=question).order_by('answer_code')
        num_choice = choiceSet.count()
        results = [[0]*5 for i in range(num_choice)]
        results2 = []
        results2.append(question.pk)

        num_responses = attemptSet.count()
        correct_responses = attemptSet.filter(is_correct=True).count()
        if num_responses > 0:
            correct_ratio = int((correct_responses/float(num_responses))*100)
            results2.append(correct_responses)
            results2.append(num_responses - correct_responses)
            results2.append(correct_ratio)
            results2.append(DifficultyLevel(correct_ratio))
        else:
            results2.append(0)
            results2.append(0)
            results2.append(0)
            results2.append("N/E")
        index.append(results2)


        i=0
        for choice in choiceSet:
            answer_set = attemptSet.filter(user_answer__contains = choice.answer_code)
            temp = answer_set.count()

            if question.question_type == 2 and i == 0:
                results[i][0] = "False"
                results[i][1] = temp
                results[i][2] = question.pk
            elif question.question_type == 2 and i == 1:
                results[i][0] = "True"
                results[i][1] = temp
                results[i][2] = question.pk
            else:
                results[i][0] = Transfer(i+1)
                results[i][1] = temp
                results[i][2] = question.pk
            if num_responses > 0:
                results[i][3] = int((temp/float(num_responses))*100)

            if choice.is_correct:
                results[i][4] = 1

            else:
                results[i][4] = 0
            i = i + 1

        stats.append(results)


    return render(request, 'mcq/asst_stats.html',locals())

@transaction.atomic
def McqEdit(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    choiceSet =  Choice.objects.all().filter(question=question).order_by('answer_code')
    choiceNum = choiceSet.count()
    choiceCounter = choiceNum+1
    optionNum = question.question_type
    subject_list = Category.objects.order_by("category_code")
    counter=0

    errors= []

    category = None
    question_text = None
    explanation = None
    hint = None


    choice_counter = None
    choice_text = None
    correct_answer = None
    answer_code = None
    explanation = None
    answer_list = ""


    if request.method == 'POST':

        if not request.POST.get('subject'):
            errors.append('Please fill out subject name.')
        else:
            category = request.POST.get('subject')

        if not request.POST.get('questiontext'):
            errors.append('Please fill out question text.')
        else:
            question_text = request.POST.get('questiontext')

        if not request.POST.get('explanation'):
            errors.append('Please fill out explanation.')
        else:
            explanation = request.POST.get('explanation')


        if optionNum is not None:
            
            try:
                with transaction.atomic():
                    
                    question.question_text = question_text
                    question.explanation = explanation
                    question.save()
                    category = Category.objects.filter(category_code=category)
                    question.category = category
                    question.save()
                    for choice in choiceSet:
                        choice.delete()

                    if optionNum == 0 or optionNum == 2:
                        if request.POST.get('counter'):
                           
                            choice_counter = request.POST.get('counter')
                            b = choice_counter
                            for i in range(1,int(b)+1):
                                print(i)
                                new_choice = Choice()
                                new_choice.question = question
                               
                                correct_answer = request.POST.get('optionsRadios')
                                code = Transfer(i)
                                new_choice.answer_code = code

                                choice_str = "choicetext"+str(i)
                                choice_text = request.POST.get(choice_str) 
                                new_choice.choice_text = choice_text
                                print("choicetest is %s",choice_text)

                                if "option"+str(i) == correct_answer:
                                    new_choice.is_correct = True
                                    answer_list = answer_list+code
                                else:
                                    new_choice.is_correct = False
                                new_choice.save()
                            question.correct_answer = answer_list
                            question.save()
                            print(answer_list)

                        return HttpResponseRedirect('/mcq/admin/statistics/mcq/%s/' %question.id)

                    if optionNum == 1:
                        if request.POST.get('counter'):
                            choice_counter = request.POST.get('counter')
                            b = choice_counter
                            for i in range(1,int(b)+1):
                                new_choice = Choice()
                                new_choice.question = question

                                code = Transfer(i)
                                new_choice.answer_code = code

                                choice_str = "choicetext"+str(i)
                                choice_text = request.POST.get(choice_str) 
                                new_choice.choice_text = choice_text

                                answer_str = "optionsCheckbox"+str(i)

                                if not request.POST.get(answer_str):
                                    new_choice.is_correct = False
                                else:
                                    new_choice.is_correct = True
                                    answer_list = answer_list+code

                                new_choice.save()
                            question.correct_answer = answer_list
                            question.save()

                        return HttpResponseRedirect('/mcq/admin/statistics/mcq/%s/' %question.id)

                    if optionNum == 3:

                        new_mcq.input_answer = request.POST.get('inputanswer')
                        new_mcq.save()

                        return HttpResponseRedirect('/mcq/admin/mcqcollection')


            except IntegrityError as e:
                errors.append(e.message)
                return render_to_response('mcq/mcq_creation.html',{'errors': errors,'optionNum':optionNum,'subject_list':subject_list},context_instance=RequestContext(request))


    print(optionNum)
    return render(request, 'mcq/mcq_edit.html',locals())




@transaction.atomic
def McqReplica(request, asst_id,question_id):
    question = get_object_or_404(Question, pk=question_id)
    choiceSet =  Choice.objects.all().filter(question=question).order_by('answer_code')
    choiceNum = choiceSet.count()
    choiceCounter = choiceNum+1
    optionNum = question.question_type
    subject_list = Category.objects.order_by("category_code")
    counter=0

    errors= []

    category = None
    question_text = None
    explanation = None
    hint = None


    choice_counter = None
    choice_text = None
    correct_answer = None
    answer_code = None
    explanation = None
    answer_list = ""

    r_question = None
    asst = get_object_or_404(Assessment, pk=asst_id)

    if request.method == 'POST':

        if not request.POST.get('subject'):
            errors.append('Please fill out subject name.')
        else:
            category = request.POST.get('subject')

        if not request.POST.get('questiontext'):
            errors.append('Please fill out question text.')
        else:
            question_text = request.POST.get('questiontext')

        if not request.POST.get('explanation'):
            errors.append('Please fill out explanation.')
        else:
            explanation = request.POST.get('explanation')


        if optionNum is not None:
            
            try:
                with transaction.atomic():
                    asst.question.remove(question)
                    r_question = Question()
                    r_question.pub_date = timezone.now()
                    r_question.question_text = question_text +" - replica"
                    r_question.explanation = explanation
                    r_question.is_replica = True
                    r_question.save()
                    category = Category.objects.filter(category_code=category)
                    r_question.category = category
                    r_question.save()
                    asst.question.add(r_question)
                    asst.save()

                    for choice in choiceSet:
                        choice.delete()

                    if optionNum == 0 or optionNum == 2:
                        if request.POST.get('counter'):
                           
                            choice_counter = request.POST.get('counter')
                            b = choice_counter
                            for i in range(1,int(b)+1):
                                print(i)
                                new_choice = Choice()
                                new_choice.question = r_question
                               
                                correct_answer = request.POST.get('optionsRadios')
                                code = Transfer(i)
                                new_choice.answer_code = code

                                choice_str = "choicetext"+str(i)
                                choice_text = request.POST.get(choice_str) 
                                new_choice.choice_text = choice_text
                                print("choicetest is %s",choice_text)

                                if "option"+str(i) == correct_answer:
                                    new_choice.is_correct = True
                                    answer_list = answer_list+code
                                else:
                                    new_choice.is_correct = False
                                new_choice.save()
                            r_question.correct_answer = answer_list
                            r_question.save()
                            print(answer_list)

                        return HttpResponseRedirect('/mcq/admin/statistics/asst/%s/' %asst.id)

                    if optionNum == 1:
                        if request.POST.get('counter'):
                            choice_counter = request.POST.get('counter')
                            b = choice_counter
                            for i in range(1,int(b)+1):
                                new_choice = Choice()
                                new_choice.question = r_question

                                code = Transfer(i)
                                new_choice.answer_code = code

                                choice_str = "choicetext"+str(i)
                                choice_text = request.POST.get(choice_str) 
                                new_choice.choice_text = choice_text

                                answer_str = "optionsCheckbox"+str(i)

                                if not request.POST.get(answer_str):
                                    new_choice.is_correct = False
                                else:
                                    new_choice.is_correct = True
                                    answer_list = answer_list+code

                                new_choice.save()
                            r_question.correct_answer = answer_list
                            r_question.save()

                        return HttpResponseRedirect('/mcq/admin/statistics/asst/%s/' %asst.id)

                    if optionNum == 3:

                        new_mcq.input_answer = request.POST.get('inputanswer')
                        new_mcq.save()

                        return HttpResponseRedirect('/mcq/admin/mcqcollection')


            except IntegrityError as e:
                errors.append(e.message)
                return render_to_response('mcq/mcq_replica.html',{'errors': errors,'optionNum':optionNum,'subject_list':subject_list},context_instance=RequestContext(request))


    print(optionNum)
    return render(request, 'mcq/mcq_replica.html',locals())



@csrf_exempt
def McqStatsView(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    num_choice = 0
    num_responses = 0
    num_respondents = 0
    results = []

    attemptSet = None
    correct_ratio = 0
    incorrect_ratio = 0
    correct_num = 0
    incorrect_num = 0
    public_result=None

    choiceSet =  Choice.objects.all().filter(question=question).order_by('answer_code')
    assignmentRelatedList = Assessment.objects.filter(question__pk=question_id)


    if request.POST.has_key('delete'):
        if not assignmentRelatedList:
            question.delete()
            choiceSet.delete()
            attemptSet = QuestionAttempt.objects.filter(question = question)
            for attempt in attemptSet:
                temp = QuestionAttempt.objects.get(id=attempt.id)
                attempt.delete()

        return HttpResponseRedirect('/mcq/admin/mcqcollection')


    print(assignmentRelatedList)

    if choiceSet:
        num_choice = choiceSet.count()

        results = [[0]*3 for i in range(num_choice)]

        attemptSet = QuestionAttempt.objects.filter(question = question)
        print(attemptSet)
        #b = QuestionAttempt.objects.all().values('user').annotate(total=Count('user'))
        num_respondents = attemptSet.values('user').distinct().count()

        num_responses = attemptSet.count()
        correct_num = attemptSet.filter(user_answer= question.correct_answer).count()
        incorrect_num = num_responses - correct_num
        i=0
        for choice in choiceSet:
            temp = attemptSet.filter(user_answer__contains = choice.answer_code).count()
            if question.question_type == 2 and i == 0:
                results[i][0] = "False"
                results[i][1] = temp
                results[i][2] = choice.votes
            elif question.question_type == 2 and i == 1:
                results[i][0] = "True"
                results[i][1] = temp
                results[i][2] = choice.votes
            else:
                results[i][0] = Transfer(i+1)
                results[i][1] = temp
                results[i][2] = choice.votes

            i = i + 1

        if num_responses > 0:

            correct_ratio = round((correct_num/float(num_responses))*100,1)
            incorrect_ratio = round((incorrect_num/float(num_responses))*100,1)

        public_result = PublicResult.objects.get(question=question.id)


        
    return render(request, 'mcq/mcq_stats.html', {'question': question,'public_result':public_result,'results':results,'num_responses':num_responses,'correct_num':correct_num,'incorrect_num':incorrect_num,'correct_ratio':correct_ratio,'incorrect_ratio':incorrect_ratio,"choice_set":choiceSet,"num_respondents":num_respondents,"asstRelatedList":assignmentRelatedList})


class McqStatsView1(generic.DetailView):
    model = Question
    template_name = 'mcq/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        for choice in question.choice_set.all:
            question=QuesstionAttempt.objects.filter(user_answer__exact=choice.answer_code).count()



        return question



class ResultsView(generic.DetailView):
    model = Question
    template_name = 'mcq/results.html'




def UserAsstResult(request,assessment_id):
    asst = get_object_or_404(Assessment, pk=assessment_id)
    asst_result = AsstResult.objects.get(user=request.user,assessment=asst)
    p = QuestionAttempt.objects.select_related('question').filter(user=request.user,assessment=asst).order_by("id")
    return render(request, 'mcq/user_asst_result.html', locals())

@transaction.atomic
def UserAsstDetail(request, assessment_id):
    asst = get_object_or_404(Assessment, pk=assessment_id)
    p=asst.question.all().order_by("id")
    print(p)


    errors= []
    score = 0

    if request.method == 'POST':
        try:
            with transaction.atomic():
                for question in p:
            #checkbox
                    if question.question_type == 1:

                        new_question_attempt = QuestionAttempt()
                        new_question_attempt.assessment = asst
                        new_question_attempt.user = request.user
                        new_question_attempt.question = question
               
                        question_ref = "question"+str(question.id)
                        user_answer_list = request.POST.getlist(question_ref)
                        print(user_answer_list)
                        user_answer=""
                        for code in user_answer_list:
                            user_answer = user_answer+code
                        new_question_attempt.user_answer = user_answer

                        if user_answer == question.correct_answer:
                            new_question_attempt.is_correct = True
                            score += 1
                        else:
                            new_question_attempt.is_correct = False
                        new_question_attempt.save()


                    if question.question_type == 0 or question.question_type == 2:
                        new_question_attempt = QuestionAttempt()
                        new_question_attempt.assessment = asst
                        new_question_attempt.user = request.user
                        new_question_attempt.question = question  
                             
                        question_ref = "question"+str(question.id)
                        user_answer= request.POST.get(question_ref)
                        print(user_answer)

                        new_question_attempt.user_answer = user_answer

                        if user_answer == question.correct_answer:
                            new_question_attempt.is_correct = True
                            score += 1
                        else:
                            new_question_attempt.is_correct = False
                        new_question_attempt.save()

                assignedAsst = AssignedAsst.objects.get(asst=asst,user=request.user)
                assignedAsst.is_done = True
                assignedAsst.save()


                new_result = AsstResult()
                new_result.assessment = asst
                new_result.user = request.user
                new_result.score = score
                new_result.grade = score
                new_result.save()



                userprofile = UserProfile.objects.get(user=request.user)
                userprofile.num_completed_asst  += 1
                
                userprofile.save()

        except IntegrityError as e:
            errors.append(e.message)

        return HttpResponseRedirect('/mcq/asstresult/%s/' %asst.id)


    return render(request, 'mcq/user_asst_detail.html', locals())


def ajax_deal(request):  
    return HttpResponse("hello")  

@csrf_exempt
def BundleDetail(request, bundle_id):
    bundle = get_object_or_404(Bundle, pk=bundle_id)
    p = bundle.question.all()

    if request.method == 'POST':
        if request.POST.has_key('delete'):
            bundle.delete()
            return HttpResponseRedirect('/mcq/admin/bundlecollection/')
            
        else:
            question = get_object_or_404(Question, pk=request.POST.get("id"))
            bundle.question.remove(question)
            payload = {'success': True}
            return HttpResponse(json.dumps(payload), content_type='application/json')

    return render(request, 'mcq/bundle_detail.html', locals())



def vote(request, question_id):
    p = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'mcq/detail.html', {
            'question': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('mcq:results', args=(p.id,)))