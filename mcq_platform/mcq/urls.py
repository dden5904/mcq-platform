from django.conf.urls import url

from . import views

# urlpatterns = [
#     url(r'^$', views.index, name='index'),
#     # ex: /polls/5/
#     url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
#     # ex: /polls/5/results/
#     url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
#     # ex: /polls/5/vote/
#     url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
# ]

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^asstlist/$', views.AsstListView.as_view(), name='asstlist'),
    url(r'^asstresult/$', views.AsstResultView.as_view(), name='asstresult'),
    url(r'^asstdetail/(?P<assessment_id>[0-9]+)/$', views.UserAsstDetail, name='userasstdetail'),
    url(r'^asstresult/(?P<assessment_id>[0-9]+)/$', views.UserAsstResult, name='userasstresult'),

    url(r'^admin/$', views.AdminIndex, name='mcqadmin'),
    
    url(r'^admin/labellist/$', views.SubjectListView, name='subjectlist'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),

    url(r'^admin/labellist/create_label/$',views.SubjectCreation,name = 'labelcreation'),
    url(r'^admin/labellist/edit/(?P<label_id>[0-9]+)/$',views.LabelEdit,name = 'labeledit'),


    url(r'^index/$', views.index, name='index'),
    url(r'^hello/$', views.hello, name='hello'),

    url(r'^profile/(.+)/$', views.profile,name="userprofile"),

    url(r'^admin/mcqcollection/$', views.McqCollectionView.as_view(), name='mcqcollection'),
    url(r'^admin/mcqcollection/create_mcq/$',views.McqOption,name = 'mcqoption'),
    url(r'^admin/mcqcollection/edit/(?P<question_id>[0-9]+)/$',views.McqEdit,name = 'mcqedit'),
    url(r'^admin/mcqcollection/replica/(?P<asst_id>[0-9]+)/(?P<question_id>[0-9]+)/$',views.McqReplica,name = 'mcqreplica'),
   
    url(r'^admin/mcqcollection/create_mcq/(?P<option>[0-3]{1})/$',views.McqCreation,name = 'mcqcreation'),
 
    url(r'^admin/bundlecollection/create_bundle/$',views.BundleCreation,name = 'bundlecreation'),
    url(r'^admin/bundlecollection/$', views.BundleCollectionView.as_view(), name='bundlecollection'),
    url(r'^admin/bundlecollection/(?P<bundle_id>[0-9]+)/$',views.BundleDetail,name = 'bundlecdetail'),
    url(r'^admin/bundlecollection/edit/(?P<bundle_id>[0-9]+)/$',views.BundleEdit,name = 'bundleedit'),
  
    url(r'^admin/asstcollection/create_asst/$',views.AsstCreation,name = 'asstcreation'),
    url(r'^admin/asstallocation/$',views.AsstAllocation,name = 'asstallocation'),
    url(r'^admin/asstcollection/$', views.AsstCollectionView.as_view(), name='asstcollection'),
    url(r'^admin/asstcollection/edit/(?P<asst_id>[0-9]+)/$',views.AsstEdit,name = 'asstedit'),

    url(r'^admin/statistics/mcq/(?P<question_id>[0-9]+)/$', views.McqStatsView, name='mcqstats'),
    url(r'^admin/statistics/asst/(?P<assessment_id>[0-9]+)/$', views.AsstStatsView, name='asststats'),
    url(r'^ajax_deal/$',views.ajax_deal,name='ajax'),
    url(r'^ccc/$',views.ccc,name="ccc"),

    url(r'^tt/$',views.tt,name="tt"),

    url(r'^embed/(?P<question_id>[0-9]+)/$', views.EmbedMcq, name='embedmcq'),

]