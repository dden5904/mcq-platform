from django.db import models
from django.db import models
from django.utils import timezone
import datetime
# Create your models here.


# For storing user profile
from django.contrib.auth.models import User
from django.db.models.signals import post_save




class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    # Other fields here
    num_completed_asst = models.IntegerField(default = 0)
    num_incomplete_asst = models.IntegerField(default = 0)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)


class Category(models.Model):
    category_code = models.CharField(max_length = 8,default = "11")
    category_name = models.CharField(max_length = 100)

    def __str__(self):              # __unicode__ on Python 2
        return self.category_code

ASST_CHOICES = (
    (0, 'practical exercise'),
    (1, 'formal exam'),
)

ASST_STATUS = (
    (0, 'OPEN'),
    (1, 'CLOSED'),
)

QUESTION_TYPES = (
    (0,'Single Answers'),
    (1,'Multiple Answers'),
    (2,'True/False'),
    (3,'Input'),
    (4,'undefined')
)

#feedback
class Question(models.Model):
    category = models.ManyToManyField(Category)
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    question_type = models.IntegerField(choices = QUESTION_TYPES,default = 0)
    correct_answer = models.CharField(max_length =100,default=0)
    explanation = models.CharField(max_length =100,default="undefined")
    input_answer = models.CharField(max_length = 100,default=0)
    hints = models.CharField(max_length =100,default="No hints.")
    is_replica = models.BooleanField(default=False)

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

    def __str__(self):              # __unicode__ on Python 2
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question)    
    answer_code = models.CharField(max_length = 10)
    is_correct = models.BooleanField(default= False)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):              # __unicode__ on Python 2
        return self.choice_text +" "+str(self.votes)

class PublicResult(models.Model):
    question = models.ForeignKey(Question)
    num_of_correct_attempts = models.IntegerField(default=0)
    num_of_incorrect_attempts = models.IntegerField(default=0)
    def __str__(self):              # __unicode__ on Python 2
        return self.question.question_text


def create_question(sender, instance, created, **kwargs):
    if created:
        PublicResult.objects.create(question=instance)

post_save.connect(create_question, sender=Question)

#total score out of
class Bundle(models.Model):
    question = models.ManyToManyField(Question)
   # category = models.ForeignKey(Category)
    bundle_name = models.CharField(max_length = 100)
    bundle_desc = models.CharField(max_length = 200,default="undefined")
    pub_date = models.DateTimeField('date published')
    total_question_num = models.IntegerField(default=0)
   # bundle_type = models.IntegerField(choices = BUNDLE_CHOICES,default = 0)
    #question_total_num = models.IntegerField(default = 1)
    #total_allocated = models.IntegerField(default = 0)
    #completed_num = models.IntegerField(default = 0)
    #pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.bundle_name



class Assessment(models.Model):
    question = models.ManyToManyField(Question, related_name='asstquestion')
    publisher = models.ForeignKey(User)
    category = models.ForeignKey(Category,null=True)
    assessment_name = models.CharField(max_length = 100,default=0)
    assessment_type = models.IntegerField(choices = ASST_CHOICES,default = 0)
    assessment_description = models.CharField(max_length = 200,default= "undefined")
    duration = models.IntegerField(default = 0)#mins  
    mcq_num = models.IntegerField(default = 0)
    points = models.IntegerField(default = 0)
    pub_date = models.DateTimeField('date published',default=None)
    deadline = models.DateTimeField('deadline')
    status = models.IntegerField(choices = ASST_STATUS,default = 0)
    


    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

    def __str__(self):
        return self.assessment_name

class ChoiceAsstStats(models.Model):
    choice = models.ForeignKey(Choice)
    question = models.ForeignKey(Question)
    asst = models.ForeignKey(Assessment)
    votes = models.IntegerField(default=0)
    def __str__(self):              # __unicode__ on Python 2
        return self.choice.choice_text

class AssessmentQuestion(models.Model):
    asst_question = models.ForeignKey(Assessment)
    question = models.ForeignKey(Question)
    question_type = models.IntegerField(default=4)
   



class QuestionAttempt(models.Model):
    assessment = models.ForeignKey(Assessment)
    user = models.ForeignKey(User)
    question = models.ForeignKey(Question)
    user_answer = models.CharField(max_length = 10)
    is_correct = models.NullBooleanField(null=True)
    def __str__(self):
        return self.assessment.assessment_name + " " + self.user.username + " " + str(self.question.pk)


class AsstResult(models.Model):
    assessment = models.ForeignKey(Assessment)
    user = models.ForeignKey(User)
    score = models.IntegerField(default = 0)
    grade = models.CharField(max_length = 50,default="Undefined" )
    def __str__(self):
        return str(self.user.username)+"-"+str(self.assessment.assessment_name)



class group(models.Model):
    group_name = models.CharField(max_length = 20)
    creation_date = models.DateTimeField('date published')


class AllocationRecord(models.Model):
    asst = models.ForeignKey(Assessment)
    staff = models.ForeignKey(User)
    when = models.DateTimeField('Date allocated')
    def __str__(self):
        return str(self.staff.username)+"-"+str(self.asst.assessment_name)

class AssignedAsst(models.Model):
    asst = models.ForeignKey(Assessment)
    user = models.ForeignKey(User, related_name='to_user')
    from_user = models.ForeignKey(User, related_name='from_user')
    is_done = models.BooleanField(default= False) 
    def __str__(self):
        return (str(self.user.pk)+str(self.asst.assessment_name))

    def getAsstName(self):
        return str(self.asst.assessment_name)

    def dayCounter(self):
        left = self.asst.deadline - timezone.now()

        if timezone.now() < self.asst.deadline :
            if(left.days > 0):
                return str(left.days) + " day(s) left"
            elif ((left.seconds//3600) >0):
                return str((left.seconds//3600)) + " hr(s) left"
            elif ((left.seconds//60)%60 >0):
                return str((left.seconds//60)%60 ) + " min(s) left"
            else:
                return "EXPIRED"
        else:
            return "EXPIRED"

    def getclassfication(self):

        left = self.asst.deadline - timezone.now()

        if timezone.now() < self.asst.deadline :
            if(left.days < 3):
                return "URGENT"
            elif (left.days <=0):
                return "EXPIRED"
            else:
                return "OPEN"
        else:
            return "OPEN"

    def was_expired(self):
        return (self.asst.deadline - timezone.now())>0
    was_expired.boolean = False

class AssignedGroup(models.Model):
    group = models.ForeignKey(group)
    asst = models.ForeignKey(Assessment)



class Event(models.Model):
    from_user = models.ForeignKey(User, related_name='relationships')
    to_user = models.ForeignKey(User, related_name='related_to')
    event = models.CharField(max_length="50")
    when = models.DateTimeField('Date happened')

    def duration(self):
        duration = timezone.now() - self.when

        if (duration.days > 0):
            return str(duration.days) +" days ago"
        elif ((duration.seconds//3600) >0):
            return str((duration.seconds//3600)) + " hr(s) ago"  
        elif ((duration.seconds//60)%60 >0):
            return str((duration.seconds//60)%60 ) + " min(s) ago"      
        elif (duration.seconds < 60):
            return str(duration.seconds) +" seconds ago"
        else:
            return "UNDFINED"

    def __str__(self):
        return str(self.from_user.username) + " " +str(self.to_user.username) +" "+str(self.event)



