# Wiki Page #
https://bitbucket.org/dden5904/mcq-platform/wiki/Home

# Demo Video #
http://www.youtube.com/watch?v=hdu4h_ju6Tg

# README #

This README documents the process required to clone the repository and launch the application.

## Project Description ##

The evolution of online testing is evident in the replacement of paper based assessments with electronic assessments. Online interactive testing is the process of testing via electronic assessments, which allows interaction with multiple choice questions, short responses, essays and other assessment formats. Multiple Choice Question (MCQ) is one of easiest assessment format and it has been used everywhere for both formative and summative assessment. It’s often appeared during online learning. The use of MCQ-based assessment does not only for education reason but also for many social aspects such as demographic survey, psychological test and self-testing etc. However, a non-computer person usually has a difficulty to produce a MCQ-based online assessment due to the lack of tech-savvy knowledge, so in this case, creating a MCQ-based assessment can be time consuming and possibly expensive. 

The MCQ Platform project focuses on developing a system which aims to assist in instructor authoring for their own MCQ-based online assessment. The system also aims to assist in online testing. The MCQ platform project is using Django Framework to build a web-based system. As an output of assessment result, our platform additionally shows all obtained assessment feedback with a number of visualised chartings through a user-friendly interface that simplifies the analysis.

### To run the server: ###
1. Install Python (2.7.x) 
2. Install Pip http://pip.readthedocs.org/en/latest/installing.html
3. Install Django ```pip install django```

```
#!bash
go to project directory with admin permission and run the command:
python manage.py runserver
//view @ url localhost:8000 or 127.0.0.1:8000
```


## TOOLS and Technologies used in this application##

* Django
* Python
* Bootstrap for front-end design
* PostgreSQL
* Ajax
* Restful design